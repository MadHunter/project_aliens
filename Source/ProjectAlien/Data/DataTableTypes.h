// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Engine/EngineTypes.h"
#include "DataTableTypes.generated.h"

USTRUCT(BlueprintType)
struct FBulletHitSurfaceData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	FBulletHitSurfaceData() : Durability(100), RicochetProbability(10.0f), RicochetSpeedDamping(60.0f), RicochetMinAngle(15.0f)
	{}

	/**
	 * Using for calculating piercing parameters
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin = 0, ClampMax = 100))
		float Durability;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin = 0, ClampMax = 100))
		float RicochetProbability;
	/**
	 * Speed damping after ricochet in percent
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin = 0, ClampMax = 100))
		float RicochetSpeedDamping;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin = 0, ClampMax = 100))
		float RicochetMinAngle;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UParticleSystem* HitParticles;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USoundCue* HitSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UMaterial* HitDecal;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector DecalSize;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterial* SpatterDecal;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector SpatterDecalSize;
};

USTRUCT(BlueprintType)
struct FPoolObjectData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	FPoolObjectData() 
	{}
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<UObject> PoolObject;
	/**
	 * Count of PoolObjects after pool initialization
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 StartCount = 10;
	/**
	 * Max life time of pool object. This is not an accurate value, because the pool manages lifetime with different intervals.
	 * Example: MaxLifeTime = 7.5f and pool.LifeInterval = 1.0f so EndLifeTime = 8.0f.
	 * Advice: You'd better use integer numbers.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MaxLifeTime = 5.0f;
	/**
	 * Increase pool size after all current object is out
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 ResizeCount = 5;

};


USTRUCT(BlueprintType)
struct FBoneHitData
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DamageMultiplication;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 bWeakPoint;

};