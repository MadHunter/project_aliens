// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "ProjectAlienHUD.generated.h"

UCLASS()
class AProjectAlienHUD : public AHUD
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, Category = "Widgets")
		TSubclassOf<class UWeaponViewWidget> WeaponViewWidgetClass;
	
public:	
	void BeginPlay() override;
	AProjectAlienHUD();
	FORCEINLINE UWeaponViewWidget* GetWeaponView() const { return WeaponViewWidget; }
private:	

	UWeaponViewWidget* WeaponViewWidget;
};

