// Fill out your copyright notice in the Description page of Project Settings.


#include "BulletBase.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Core/Subsystems/BulletPoolSubsystem.h"
#include "Character/CharacterBase.h"

void ABulletBase::BeginPlay()
{
	Super::BeginPlay();
	BulletPool = GetGameInstance()->GetSubsystem<UBulletPoolSubsystem>();
}

// Sets default values
ABulletBase::ABulletBase()
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &ABulletBase::OnHit);		// set up a notification for when this component hits something blocking
	CollisionComp->OnComponentBeginOverlap.AddDynamic(this, &ABulletBase::OnBeginOverlap);
	CollisionComp->OnComponentEndOverlap.AddDynamic(this, &ABulletBase::OnEndOverlap);
	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 30000.f;
	ProjectileMovement->MaxSpeed = 30000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;
	PrimaryActorTick.bCanEverTick = false;
}

void ABulletBase::SetSpawnParams(const FBulletSpawnParams& SpawnParams)
{
	GetProjectileMovement()->InitialSpeed = SpawnParams.StartSpeed;
	GetProjectileMovement()->MaxSpeed = SpawnParams.StartSpeed * 1.2f;
	DamageData.BulletDamage = SpawnParams.Damage;
	DamageData.PiercingPower = SpawnParams.PiercingPower;
	DamageData.StoppingPower = SpawnParams.StoppingPower;
	DamageData.HitImpulse = SpawnParams.HitImpusle;
	OnBulletHit = SpawnParams.OnBulletHit;
}

void ABulletBase::AddIgnoredActor(AActor* OtherActor)
{
	CollisionComp->IgnoreActorWhenMoving(OtherActor, true);
}


void ABulletBase::OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& SweepResult)
{
	ApplyHitEffect(SweepResult);
	/*if (BulletPool)
		BulletPool->ReturnToPool(this);*/
	Destroy();
}

void ABulletBase::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ApplyHitEffect(SweepResult);
	ACharacterBase* Character = Cast<ACharacterBase>(OtherActor);
	if (Character)
	{
		Character->TakeDamage(DamageData, /*OUT*/ HitData);
		OnBulletHit.Broadcast(HitData);
	}
	Destroy();
}

void ABulletBase::OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void ABulletBase::SetActive(bool bIsActive)
{
	SetActorTickEnabled(bIsActive);
	SetActorHiddenInGame(!bIsActive);
	SetActorEnableCollision(bIsActive);
	ProjectileMovement->SetComponentTickEnabled(bIsActive);
	ProjectileMovement->bSimulationEnabled = bIsActive;
}
