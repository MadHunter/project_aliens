// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Types/BulletData.h"
#include "BulletBase.generated.h"

UCLASS(config = Game)
class PROJECTALIEN_API ABulletBase : public AActor
{
	GENERATED_BODY()

	/**
	 * Keeps info about last bullet Hit
	 */
	UPROPERTY(VisibleAnywhere)
		FBulletHitData HitData;
	/**
	 * Keeps info about bullet damage
	 */
	UPROPERTY(VisibleAnywhere)
		FBulletDamageData DamageData;
	
	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
		class USphereComponent* CollisionComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
		class UProjectileMovementComponent* ProjectileMovement;
	UPROPERTY()
		class UBulletPoolSubsystem* BulletPool;
protected:
	void BeginPlay() override;
public:
	// Sets default values for this actor's properties
	ABulletBase();
	virtual void SetSpawnParams(const FBulletSpawnParams& SpawnParams);

	UFUNCTION(BlueprintCallable)
		void AddIgnoredActor(AActor* OtherActor);
	UFUNCTION(BlueprintImplementableEvent, Category = "Bullet")
		void ApplyHitEffect(const FHitResult& SweepResult);
	UFUNCTION()
		void OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& SweepResult);
	UFUNCTION()
		void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION()
	void SetActive(bool bIsActive);
	
	FORCEINLINE class USphereComponent* GetCollisionComp() const { return CollisionComp; }
	FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }
private:
	FBulletHitEvent OnBulletHit;
};
