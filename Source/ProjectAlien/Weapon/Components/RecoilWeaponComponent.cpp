// Fill out your copyright notice in the Description page of Project Settings.


#include "RecoilWeaponComponent.h"
#include "Engine/World.h"

// Sets default values for this component's properties
URecoilWeaponComponent::URecoilWeaponComponent(): RecoilAngle(0.0f), CurrentRecoilAngle(0.0f), WeaponRecoil(0.0f)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

void URecoilWeaponComponent::IncreaseRecoil(bool bIronsight)
{
	const FScatterParams& Params = bIronsight ? RecoilAimParams : RecoilParams;
	float NewRecoil = WeaponRecoil + CurrentRecoilAngle;
	WeaponRecoil = NewRecoil < Params.MaxValue ? NewRecoil : Params.MaxValue;
	FVector RecoilVector = GetDirection() * WeaponRecoil;
	IncreaseRecoilAngle(Params);
	OnRecoil().Broadcast(RecoilVector);
}


void URecoilWeaponComponent::RelaxRecoil(bool bIronsight, bool bShooting)
{
	const FScatterParams& Params = bIronsight ? RecoilAimParams : RecoilParams;
	float Speed = bShooting ? Params.RelaxSpeedWhileShooting : Params.RelaxSpeed;
	float NewRecoil = WeaponRecoil - (Speed * GetWorld()->GetDeltaSeconds());
	WeaponRecoil = NewRecoil > Params.MinValue ? NewRecoil : Params.MinValue;
	DecreaseRecoilAngle(bShooting, Params);
}

// Called when the game starts
void URecoilWeaponComponent::BeginPlay()
{
	Super::BeginPlay();
}

FVector URecoilWeaponComponent::GetDirection() const
{
	FVector Direction = -FVector::ForwardVector;
	if (RecoilAngle > 0)
	{
		float Angle = FMath::RandRange(-RecoilAngle, RecoilAngle);
		Direction = Direction.GetSafeNormal().RotateAngleAxis(Angle, FVector(0, 0, 1));
	}
	return Direction.GetSafeNormal();
}

void URecoilWeaponComponent::IncreaseRecoilAngle(const FScatterParams& Params)
{
	float Increment = CurrentRecoilAngle + Params.IncAcceleration;
	CurrentRecoilAngle = FMath::Min(Increment, Params.MaxIncrease);
}

void URecoilWeaponComponent::DecreaseRecoilAngle(bool bShooting, const FScatterParams& Params)
{
	float Speed = bShooting ? Params.IncreaseRelaxSpeedWhileShooting : Params.IncreaseRelaxSpeed;
	float Increment = CurrentRecoilAngle - (Speed * GetWorld()->GetDeltaSeconds());
	CurrentRecoilAngle = FMath::Max(Increment, Params.IncPerShot);
}
