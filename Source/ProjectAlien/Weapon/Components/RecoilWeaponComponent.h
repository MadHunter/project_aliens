// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Types/Changeable.h"
#include "Types/ScatterParams.h"
#include "RecoilWeaponComponent.generated.h"

UENUM(BlueprintType)
enum class EWeaponRecoil : uint8
{
	SideToSide,
	Up
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROJECTALIEN_API URecoilWeaponComponent : public UActorComponent
{
	GENERATED_BODY()
		using RecoilEvent = TChangeable<FVector>::FChangeEvent;

	UPROPERTY(EditAnywhere, Category = WeaponRecoil)
		FScatterParams RecoilParams;
	UPROPERTY(EditAnywhere, Category = WeaponRecoil)
		FScatterParams RecoilAimParams;
	UPROPERTY(EditAnywhere, Category = WeaponRecoil, Meta = (ClampMin = 0))
		float RecoilAngle;

	UPROPERTY(VisibleAnywhere, Category = WeaponRecoil)
		float CurrentRecoilAngle;
	UPROPERTY(VisibleAnywhere, Category = WeaponRecoil)
		float WeaponRecoil;

public:
	// Sets default values for this component's properties
	URecoilWeaponComponent();

	void IncreaseRecoil(bool bIronsight);
	void RelaxRecoil(bool bIronsight, bool bShooting);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	FORCEINLINE RecoilEvent& OnRecoil() { return OnRecoilEvent; }
	FORCEINLINE FScatterParams& GetRecoilParams() { return RecoilParams; }
	FORCEINLINE FScatterParams& GetRecoilAimParams() { return RecoilAimParams; }
private:
	/*
	 * Get normalized direction 
	 */
	FVector GetDirection() const;
	void IncreaseRecoilAngle(const FScatterParams& Params);
	void DecreaseRecoilAngle(bool bShooting, const FScatterParams& Params);

	
	RecoilEvent OnRecoilEvent;

};
