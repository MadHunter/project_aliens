// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Types/Changeable.h"
#include "Types/FireParams.h"
#include "Types/ScatterParams.h"
#include "Types/BulletData.h"
#include "WeaponBase.generated.h"

UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	Default,
	Aim,
	Reload
};

DECLARE_EVENT(AWeaponBase, FOnPlayActionEvent)

UCLASS()
class PROJECTALIEN_API AWeaponBase : public AActor
{
	GENERATED_BODY()

		using RecoilEvent = TChangeable<FVector>::FChangeEvent;

	UPROPERTY(EditAnywhere, Category = WeaponBase)
		float Damage;
	/**
	 * Weight in kilograms 
	 */
	UPROPERTY(EditAnywhere, Category = WeaponBase, Meta = (ClampMin = 0))
		float Weight;
	UPROPERTY(EditAnywhere, Category = WeaponBase, Meta = (ClampMin = 100))
		float InitialBulletSpeed;
	UPROPERTY(EditAnywhere, Category = WeaponBase, Meta = (ClampMin = 0))
		int32 MagSize;
	UPROPERTY(EditAnywhere, Category = WeaponBase, Meta = (ClampMin = 0))
		float WiggleAngle = 0.1f;
	UPROPERTY(EditAnywhere, Category = WeaponBase, meta = (Bitmask, BitmaskEnum = "EFireMode"))
		int32 FireMode;
	UPROPERTY(EditAnywhere, Category = WeaponBase)
		FFireParams FireParams;
	/**
	 * Scale camera's FOV while Aiming
	 */
	UPROPERTY(EditAnywhere, Category = WeaponBase)
		float AimScale = 1.1f;
	UPROPERTY(EditAnywhere, Category = WeaponBase)
		TSubclassOf<class ABulletBase> BulletClass;
	
	UPROPERTY(EditAnywhere, Category = WeaponScatter)
		FScatterParams ScatterParams;
	UPROPERTY(EditAnywhere, Category = WeaponScatter)
		FScatterParams ScatterAimParams;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USkeletalMeshComponent* Mesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class URecoilWeaponComponent* RecoilWeaponComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class ACharacterBase* OwnerCharacter;
	UPROPERTY()
		class UBulletPoolSubsystem* BulletPool;
public:
	TChangeable<uint8> CurFireMode;
	TChangeable<int32> CurMagSize;
	TChangeable<float> CurrentScatter;
private:

	uint32 Locked;
	uint8 bIronsight : 1;
	float WeaponScatter;
public:
	// Sets default values for this actor's properties
	AWeaponBase();
	virtual void Tick(float DeltaTime) override;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:
	UFUNCTION(BlueprintCallable, Category = Weapon)
		virtual void Equip(ACharacterBase* Character);
	UFUNCTION(BlueprintCallable, Category = Weapon)
		virtual void Unequip();
	UFUNCTION(BlueprintCallable, Category = Weapon)
		virtual void StartShooting();
	UFUNCTION(BlueprintCallable, Category = Weapon)
		virtual void StopShooting();
	UFUNCTION(BlueprintCallable, Category = Weapon)
		virtual void LockWeapon();
	UFUNCTION(BlueprintCallable, Category = Weapon)
		virtual void UnlockWeapon();
	UFUNCTION(BlueprintCallable, Category = Weapon)
		virtual void UnlockWeaponImmediately();
	UFUNCTION(BlueprintCallable, Category = Weapon)
		virtual bool IsWeaponLocked();
	UFUNCTION(BlueprintCallable, Category = Weapon)
		void SetAiming(bool bAiming);
	UFUNCTION(BlueprintImplementableEvent, Category = Weapon)
		void OnMakeShoot();
private:
	void MakeShot();
	void SpawnBullet();
	bool IsNeedToStopShooting() const;
	void IncreaseScatter();
	void DecreaseScatter(float DeltaTime);
	void ClearEventHandlers();
public:
	FORCEINLINE ACharacterBase* GetOwnerCharacter() const { return OwnerCharacter; }
	
	FORCEINLINE float GetDamage() const { return Damage; }
	FORCEINLINE float GetWeight() const { return Weight; }
	FORCEINLINE int32 GetFireModeMask() const { return FireMode; }
	FORCEINLINE int32 GetMagMaxSize() const { return MagSize; }
	FORCEINLINE float GetShotsPerMinute()const { return FireParams.ShotsPerMinute; }
	FORCEINLINE float GetAimScale()const { return AimScale; }
	FORCEINLINE float GetWiggleAngle()const { return WiggleAngle; }
	FORCEINLINE FVector GetSightOffset()const { return SightOffset; }
	
	FORCEINLINE USkeletalMeshComponent* GetMesh() const { return Mesh; }
	FORCEINLINE URecoilWeaponComponent* GetRecoilWeaponComponent() const { return RecoilWeaponComponent; }
	
	FORCEINLINE FBulletHitEvent& OnBulletHit() { return OnBulletHitCallback; }
	FORCEINLINE FOnPlayActionEvent& OnShootingBreak() { return OnShootingBreakEvent; }
	FORCEINLINE FOnPlayActionEvent& OnMagazineEmpty() { return OnMagazineEmptyEvent; }
	FORCEINLINE RecoilEvent& OnRecoil() const;
private:
	FBulletHitEvent OnBulletHitCallback;
	FOnPlayActionEvent OnShootingBreakEvent;
	FOnPlayActionEvent OnMagazineEmptyEvent;
	FVector SightOffset;
	
};
