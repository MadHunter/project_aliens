// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBase.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/World.h"
#include "Character/CharacterBase.h"
#include "Character/RaycastCheckerComponent.h"
#include "Weapon/BulletBase.h"
#include "Character/Components/CharacterRotationComponent.h"
#include "Core/Subsystems/BulletPoolSubsystem.h"
#include "Weapon/Components/RecoilWeaponComponent.h"

// Sets default values
AWeaponBase::AWeaponBase() : Damage(35), Weight(3.5f), InitialBulletSpeed(30000), MagSize(30), Locked(0), bIronsight(false), WeaponScatter(0)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bAllowTickBeforeBeginPlay = false;
	USceneComponent* NewRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("WeaponRoot"));
	RootComponent = NewRootComponent;
	NewRootComponent->SetRelativeLocation(FVector::ZeroVector);
	NewRootComponent->SetRelativeRotation(FQuat::Identity);
	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	Mesh->SetupAttachment(GetRootComponent());
	Mesh->SetRelativeLocation(FVector::ZeroVector);
	Mesh->SetRelativeRotation(FQuat::Identity);
	RecoilWeaponComponent = CreateDefaultSubobject<URecoilWeaponComponent>("RecoilWeaponComponent");
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	CurMagSize = MagSize;
	SightOffset = GetMesh() ? GetMesh()->GetSocketTransform("Ironsight", RTS_Component).GetLocation() : FVector::ZeroVector;
	BulletPool = GetGameInstance()->GetSubsystem<UBulletPoolSubsystem>();
}

void AWeaponBase::Equip(ACharacterBase* Character)
{
	this->OwnerCharacter = Character;
}

void AWeaponBase::Unequip()
{
	SetAiming(false);
	UnlockWeaponImmediately();
	this->OwnerCharacter = nullptr;
	ClearEventHandlers();
}

void AWeaponBase::StartShooting()
{
	if (!IsWeaponLocked())
	{
		FireParams.bShooting = true;
	}
}

void AWeaponBase::StopShooting()
{
	if (!IsWeaponLocked())
	{
		FireParams.bShooting = false;
		FireParams.UninterruptedShots = 0;
	}
}


// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	if (!IsWeaponLocked())
	{
		if (FireParams.bShooting)
		{
			if (GetWorld()->GetTimeSeconds() >= FireParams.LastShotTime + (60.0f / FireParams.ShotsPerMinute))
			{
				if (CurMagSize > 0)
				{
					MakeShot();
				}
				else
				{
					OnMagazineEmpty().Broadcast();
				}
			}
		}
	}
	RecoilWeaponComponent->RelaxRecoil(bIronsight, FireParams.bShooting);
	DecreaseScatter(DeltaTime);
	CurrentScatter = WeaponScatter + (OwnerCharacter ? OwnerCharacter->GetCharacterScatter() : 0);
	SightOffset = GetMesh() ? GetMesh()->GetSocketTransform("Ironsight", RTS_Component).GetLocation() : FVector::ZeroVector;
}

void AWeaponBase::LockWeapon()
{
	++Locked;
}

void AWeaponBase::UnlockWeapon()
{
	--Locked;
}

void AWeaponBase::UnlockWeaponImmediately()
{
	Locked = 0;
}

bool AWeaponBase::IsWeaponLocked()
{
	return Locked > 0;
}

void AWeaponBase::SetAiming(bool bAiming)
{
	bIronsight = bAiming;
}

void AWeaponBase::MakeShot()
{
	SpawnBullet();
	IncreaseScatter();
	RecoilWeaponComponent->IncreaseRecoil(bIronsight);
	FireParams.LastShotTime = GetWorld()->GetTimeSeconds();
	++FireParams.UninterruptedShots;
	--CurMagSize;
	OnMakeShoot();
	if (IsNeedToStopShooting())
		OnShootingBreak().Broadcast();//OwnerCharacter->StopShooting();
}

void AWeaponBase::SpawnBullet()
{
	//
	FVector Position = GetMesh()->GetSocketLocation("Muzzle");
	FVector Direction = (GetOwnerCharacter()->GetEyeRaycast()->GetTargetPoint(GetOwnerCharacter()->GetRotationComponent()->GetDeltaRotation())) - Position;
	FBulletSpawnParams SpawnParams;
	SpawnParams.Damage = GetDamage();
	SpawnParams.SpawnPosition = Position;
	SpawnParams.Rotation = Direction.ToOrientationRotator();
	SpawnParams.Scatter = CurrentScatter;
	SpawnParams.StartSpeed = InitialBulletSpeed;
	SpawnParams.Shooter = GetOwnerCharacter();
	SpawnParams.OnBulletHit = OnBulletHitCallback;
	//TODO: Add piercing and stopping power
	SpawnParams.PiercingPower = 0;
	SpawnParams.StoppingPower = 0;
	SpawnParams.HitImpusle = 100;

	/*Set Scatter to bullet*/
	FRotator SpawnRotation = SpawnParams.Rotation;
	float yawRand = FMath::RandRange(-SpawnParams.Scatter, SpawnParams.Scatter);
	SpawnRotation.Yaw += yawRand;
	float pitchRand = FMath::RandRange(-SpawnParams.Scatter, SpawnParams.Scatter);
	SpawnRotation.Pitch += pitchRand;

	ABulletBase* Round = /*BulletPool->GetBullet(BulletClass->GetFName(), Position, SpawnRotation);*/GetWorld()->SpawnActor<ABulletBase>(BulletClass, Position, SpawnRotation);
	Round->SetSpawnParams(SpawnParams);
	Round->AddIgnoredActor(this);
	Round->AddIgnoredActor(this->GetOwnerCharacter());
}

bool AWeaponBase::IsNeedToStopShooting() const
{
	switch (CurFireMode)
	{
	case (uint8)EFireMode::OneShot:
		return true;
	case (uint8)EFireMode::TwoShot:
		return FireParams.UninterruptedShots >= 2;
	case (uint8)EFireMode::ThreeShot:
		return FireParams.UninterruptedShots >= 3;
	case (uint8)EFireMode::Auto:
	default:
		return false;
	}
}

void AWeaponBase::IncreaseScatter()
{
	const FScatterParams& Params = bIronsight ? ScatterAimParams : ScatterParams;
	float NewScatter = WeaponScatter + Params.IncPerShot;
	WeaponScatter = NewScatter < Params.MaxValue ? NewScatter : Params.MaxValue;
}

void AWeaponBase::DecreaseScatter(float DeltaTime)
{
	const FScatterParams& Params = bIronsight ? ScatterAimParams : ScatterParams;
	float Speed = FireParams.bShooting ? Params.RelaxSpeedWhileShooting : Params.RelaxSpeed;
	float NewScatter = WeaponScatter - (Speed * DeltaTime);
	WeaponScatter = NewScatter > Params.MinValue ? NewScatter : Params.MinValue;
}

void AWeaponBase::ClearEventHandlers()
{
	OnRecoil().Clear();
	OnBulletHit().Clear();
	CurFireMode.OnValueChanged().Clear();
	CurMagSize.OnValueChanged().Clear();
	CurrentScatter.OnValueChanged().Clear();
	OnMagazineEmptyEvent.Clear();
	OnShootingBreakEvent.Clear();
}

AWeaponBase::RecoilEvent& AWeaponBase::OnRecoil() const
{
	return RecoilWeaponComponent->OnRecoil();
}

