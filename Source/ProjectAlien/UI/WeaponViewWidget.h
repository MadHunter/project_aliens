// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/BaseViewWidget.h"
#include "Types/BulletData.h"
#include "WeaponViewWidget.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTALIEN_API UWeaponViewWidget : public UBaseViewWidget
{
	GENERATED_BODY()
public:
	int32 MaxRows;
public:
	UFUNCTION(BlueprintImplementableEvent, Category = "WeaponView")
		void ShowCrosshairScatter(float Scatter);
	UFUNCTION(BlueprintImplementableEvent, Category = "WeaponView")
		void ShowCurrentMagSize(int32 MagSize);
	UFUNCTION(BlueprintImplementableEvent, Category = "WeaponView")
		void ShowMaxMagSize(int32 MagSize);
	UFUNCTION(BlueprintImplementableEvent, Category = "WeaponView")
		void ShowAmmoCount(int32 AmmoCount);
	UFUNCTION(BlueprintImplementableEvent, Category = "WeaponView")
		void ShowWeaponImage(class UTexture2D* Texture);
	UFUNCTION(BlueprintImplementableEvent, Category = "WeaponView")
		void ShowCrosshairHit(const FBulletHitData& HitData);
	UFUNCTION(BlueprintImplementableEvent, Category = "WeaponView")
		void ShowCrosshair();
	UFUNCTION(BlueprintImplementableEvent, Category = "WeaponView")
		void HideCrosshair();
};
