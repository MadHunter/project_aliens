// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BaseViewWidget.generated.h"

/**
 *
 */
UCLASS()
class PROJECTALIEN_API UBaseViewWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintImplementableEvent, Category = "BaseView")
		void Show();

	UFUNCTION(BlueprintImplementableEvent, Category = "BaseView")
		void Hide();
};
