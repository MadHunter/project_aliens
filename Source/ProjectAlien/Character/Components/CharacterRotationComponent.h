// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CharacterRotationComponent.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROJECTALIEN_API UCharacterRotationComponent : public UActorComponent
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, Category = Rotation)
		class UCurveFloat* RotationWeightCurve;

	UPROPERTY(EditDefaultsOnly, Category = Rotation)
		float TurnRate = 100;
	UPROPERTY(EditAnywhere, Category = Rotation)
		uint8 bSmoothLook : 1;
	UPROPERTY(EditAnywhere, Category = Rotation, Meta = (ClampMin = 0))
		float SmoothLookSensitivity = 20;
	UPROPERTY(EditAnywhere, Category = Rotation)
		uint8 bUseWeaponWeight : 1;
	UPROPERTY(VisibleAnywhere, Category = Rotation)
		FRotator NeedRotation;
	UPROPERTY(EditAnywhere, Category = RotationUtility)
		float RotationTolerance = 0.001f;
public:
	// Sets default values for this component's properties
	UCharacterRotationComponent();

	void AddPitchRotation(float Rate);
	void AddYawRotation(float Rate);
	void AddRollRotation(float Rate);
private:
	void TryRoundRotation();
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	FORCEINLINE UCurveFloat* GetRotationWeightCurve() const { return RotationWeightCurve; }
	FORCEINLINE FRotator GetDeltaRotation() const { return DeltaRotation; }
private:
	FRotator DeltaRotation;
	class ACharacterBase* OwnerCharacter;
};
