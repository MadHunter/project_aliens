// Fill out your copyright notice in the Description page of Project Settings.


#include "RecoilCharacterComponent.h"
#include "Engine/World.h"
#include "Character/CharacterBase.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"

// Sets default values for this component's properties
URecoilCharacterComponent::URecoilCharacterComponent() : bUseRecoil(true), RecoilMultiplier(1.0f), bUseForceCurve(false), RecoilForce(15), bUseRelaxCurve(false), RecoilRelax(10), RecoilState(ERecoilState::ERecoilNone)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

void URecoilCharacterComponent::MakeRecoil(FVector Recoil)
{
	if (bUseRecoil)
	{
		RecoilTargetVector = Recoil * RecoilMultiplier;
		RecoilState = ERecoilState::ERecoilForce;
	}
}

void URecoilCharacterComponent::ProcessRecoil()
{
	switch (RecoilState)
	{
	case ERecoilState::ERecoilForce:
		ProcessForce();
		break;
	case ERecoilState::ERecoilRelax:
		ProcessRelax();
		break;
	case ERecoilState::ERecoilNone:
		break;
	}
}

void URecoilCharacterComponent::ProcessForce()
{
	if (bUseForceCurve)
	{
		ProcessForceCurve();
	}
	else
	{
		ProcessForceValue();
	}
}


void URecoilCharacterComponent::ProcessForceCurve()
{
	if (RecoilForceCurve)
	{
		const float CurrentSpeed = RecoilForceCurve->GetFloatValue(FVector::Dist(RecoilTargetVector, CurrentRecoilVector));
		UpdateRecoil(CurrentSpeed, RecoilTargetVector, ERecoilState::ERecoilRelax);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Parametr bUseForceCurve has beed chosen, but there is no RecoilForceCurve."));
	}
}

void URecoilCharacterComponent::ProcessForceValue()
{
	UpdateRecoil(RecoilForce, RecoilTargetVector, ERecoilState::ERecoilRelax);
}


void URecoilCharacterComponent::ProcessRelax()
{
	if (bUseRelaxCurve)
	{
		ProcessRelaxCurve();
	}
	else
	{
		ProcessRelaxValue();
	}
}

void URecoilCharacterComponent::ProcessRelaxCurve()
{
	if (RecoilRelaxCurve)
	{
		const float CurrentSpeed = RecoilRelaxCurve->GetFloatValue(FVector::Dist(RecoilTargetVector, CurrentRecoilVector));
		UpdateRecoil(CurrentSpeed, FVector::ZeroVector, ERecoilState::ERecoilNone);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Parameter bUseRelaxCurve has beed chosen, but there is no RecoilRelaxCurve."));
	}
}

void URecoilCharacterComponent::ProcessRelaxValue()
{
	UpdateRecoil(RecoilRelax, FVector::ZeroVector, ERecoilState::ERecoilNone);
}

void URecoilCharacterComponent::UpdateRecoil(float Speed, FVector TargetVector, ERecoilState NextState)
{
	FVector Delta;
	if (!CurrentRecoilVector.Equals(TargetVector, 0.1f))
	{
		Delta = (TargetVector - CurrentRecoilVector).GetSafeNormal() * FMath::Clamp(GetWorld()->GetDeltaSeconds() * Speed, 0.0f, 1.0f);
	}
	else if (CurrentRecoilVector != TargetVector)
	{
		Delta = TargetVector - CurrentRecoilVector;
		RecoilState = NextState;
	}
	OwnerCharacter->AddControllerPitchInput(Delta.X);
	OwnerCharacter->AddControllerYawInput(Delta.Y);
	CurrentRecoilVector += Delta;
}

// Called when the game starts
void URecoilCharacterComponent::BeginPlay()
{
	Super::BeginPlay();
	OwnerCharacter = Cast<ACharacterBase>(GetOwner());
}


// Called every frame
void URecoilCharacterComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	ProcessRecoil();
}

