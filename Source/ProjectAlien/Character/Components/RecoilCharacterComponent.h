// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "RecoilCharacterComponent.generated.h"

UENUM()
enum class ERecoilState : uint8
{
	ERecoilForce,
	ERecoilRelax,
	ERecoilNone
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROJECTALIEN_API URecoilCharacterComponent : public UActorComponent
{
	GENERATED_BODY()
		UPROPERTY(EditAnywhere, Category = RecoilBase)
		uint8 bUseRecoil : 1;
	UPROPERTY(EditAnywhere, Category = RecoilBase, Meta = (ClampMin = 0.01f, ClampMax = 5.0f))
		float RecoilMultiplier;
	UPROPERTY(EditAnywhere, Category = RecoilForce)
		uint8 bUseForceCurve : 1;
	UPROPERTY(EditAnywhere, Category = RecoilForce)
		float RecoilForce;
	UPROPERTY(EditAnywhere, Category = RecoilForce)
		class UCurveFloat* RecoilForceCurve;
	UPROPERTY(EditAnywhere, Category = RecoilRelax)
		uint8 bUseRelaxCurve : 1;
	UPROPERTY(EditAnywhere, Category = RecoilRelax)
		float RecoilRelax;
	UPROPERTY(EditAnywhere, Category = RecoilRelax)
		UCurveFloat* RecoilRelaxCurve;
	UPROPERTY(VisibleInstanceOnly, Category = RecoilBase)
		FVector CurrentRecoilVector;
	UPROPERTY(VisibleInstanceOnly, Category = RecoilBase)
		FVector RecoilTargetVector;

	ERecoilState RecoilState;
	class ACharacterBase* OwnerCharacter;
public:
	// Sets default values for this component's properties
	URecoilCharacterComponent();

	UFUNCTION(BlueprintCallable)
		void MakeRecoil(FVector Recoil);
private:
	void ProcessRecoil();
	void ProcessForce();
	void ProcessForceCurve();
	void ProcessForceValue();
	void ProcessRelax();
	void ProcessRelaxCurve();
	void ProcessRelaxValue();
	void UpdateRecoil(float Speed, FVector TargetRotator, ERecoilState NextState);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


};
