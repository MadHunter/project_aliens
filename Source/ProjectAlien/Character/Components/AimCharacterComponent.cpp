// Fill out your copyright notice in the Description page of Project Settings.


#include "AimCharacterComponent.h"
#include "Character/PlayableCharacter.h"
#include "Weapon/WeaponBase.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Engine/World.h"
#include "ProjectAlienHUD.h"
#include "ui/WeaponViewWidget.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"

// Sets default values for this component's properties
UAimCharacterComponent::UAimCharacterComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	OwnerCharacter = Cast<APlayableCharacter>(GetOwner());
}


// Called when the game starts
void UAimCharacterComponent::BeginPlay()
{
	Super::BeginPlay();
	DefaultCameraFOV = OwnerCharacter->GetCameraComponent()->FieldOfView;
	DefaultMovementSpeed = OwnerCharacter->GetCharacterMovement()->MaxWalkSpeed;
}


// Called every frame
void UAimCharacterComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	ProcessAiming();
}

void UAimCharacterComponent::StartSighting()
{
	if (AimState != EAimState::EAimWiggle && AimState != EAimState::EAimStart)
		AimState = EAimState::EAimStart;
	if (OwnerCharacter->GetPlayerHUD())
		OwnerCharacter->GetPlayerHUD()->GetWeaponView()->HideCrosshair();
	
	TargetCameraFOV = DefaultCameraFOV / (OwnerCharacter->GetWeapon() ? OwnerCharacter->GetWeapon()->GetAimScale() : 1);
	OwnerCharacter->GetCharacterMovement()->MaxWalkSpeed = AimMovementSpeed;
	TargetWeaponLocation = OwnerCharacter->GetWeapon()->GetSightOffset();
	
	ResetCachedValues();
	CalculateWiggleTarget();
	CalculateWiggleSpeed();
}

void UAimCharacterComponent::StopSighting()
{
	if (AimState != EAimState::EAimNone && AimState != EAimState::EAimStop)
		AimState = EAimState::EAimStop;
	if (OwnerCharacter->GetPlayerHUD())
		OwnerCharacter->GetPlayerHUD()->GetWeaponView()->ShowCrosshair();
	
	TargetCameraFOV = DefaultCameraFOV;
	OwnerCharacter->GetCharacterMovement()->MaxWalkSpeed = DefaultMovementSpeed;
	TargetWeaponLocation = FVector::ZeroVector;
}

void UAimCharacterComponent::ProcessAiming()
{
	switch (AimState)
	{
	case EAimState::EAimStart:
		ProcessStartAiming();
		break;
	case EAimState::EAimStop:
		ProcessStopAiming();
		break;
	case EAimState::EAimWiggle:
		ProcessWiggleAiming();
		break;
	case EAimState::EAimNone:
	default:
		break;
	}
}

void UAimCharacterComponent::ProcessStartAiming()
{
	UpdateAiming(EAimState::EAimWiggle);
}

void UAimCharacterComponent::ProcessStopAiming()
{
	UpdateAiming(EAimState::EAimNone);
}

void UAimCharacterComponent::ProcessWiggleAiming()
{
	FVector PrevWiggle = CurrentWiggleVector;

	CurrentWiggleAlpha += CurrentWiggleSpeed * GetWorld()->GetDeltaSeconds();
	CurrentWiggleVector = FMath::Lerp(StartWiggleVector, TargetWiggleVector, FMath::Clamp(CurrentWiggleAlpha, 0.0f, 1.0f));

	if (FVector::DistXY(CurrentWiggleVector, TargetWiggleVector) < WiggleTolerance)
	{
		CurrentWiggleVector = TargetWiggleVector;
		StartWiggleVector = TargetWiggleVector;
		CurrentWiggleAlpha = 0.0f;
		CalculateWiggleTarget();
		CalculateWiggleSpeed();
	}

	FVector Delta = CurrentWiggleVector - PrevWiggle;
	OwnerCharacter->AddControllerPitchInput(Delta.X);
	OwnerCharacter->AddControllerYawInput(Delta.Y);
}

void UAimCharacterComponent::CalculateWiggleTarget()
{
	FVector Direction(FMath::RandRange(-1.0f, 1.0f), FMath::RandRange(-1.0f, 1.0f), 0.0f);
	if (TargetWiggleVector != FVector::ZeroVector)
	{
		float Angle = FMath::RandRange(90.0f, 270.0f);
		Direction = TargetWiggleVector.GetSafeNormal().RotateAngleAxis(Angle, FVector(0, 0, 1));
	}
	Direction.Normalize();
	TargetWiggleVector = Direction * (OwnerCharacter->GetWeapon() ? OwnerCharacter->GetWeapon()->GetWiggleAngle() : 0);
}

void UAimCharacterComponent::UpdateAiming(EAimState NextState)
{
	float Alpha = FMath::Clamp(GetWorld()->GetDeltaSeconds() * AimingSpeed, 0.0f, 1.0f);
	float CameraFOV = FMath::Lerp(OwnerCharacter->GetCameraComponent()->FieldOfView, TargetCameraFOV, Alpha);
	FVector WeaponPos = FMath::Lerp(OwnerCharacter->GetWeapon()->GetRootComponent()->RelativeLocation, TargetWeaponLocation, Alpha);
	if (FMath::Abs(CameraFOV - TargetCameraFOV) < AimingStopThreshold)
	{
		CameraFOV = TargetCameraFOV;
		WeaponPos = TargetWeaponLocation;
		AimState = NextState;
	}
	OwnerCharacter->GetCameraComponent()->FieldOfView = CameraFOV;
	OwnerCharacter->GetWeapon()->SetActorRelativeLocation(WeaponPos);
}

void UAimCharacterComponent::CalculateWiggleSpeed()
{

	if (bUseWiggleCurve && WiggleCurve)
	{
		float Time = FVector::DistXY(CurrentWiggleVector, TargetWiggleVector);
		CurrentWiggleSpeed = WiggleCurve->GetFloatValue(Time);
	}
	else
	{
		CurrentWiggleSpeed = WiggleSpeed;
	}
}

void UAimCharacterComponent::ResetCachedValues()
{
	TargetWiggleVector = FVector::ZeroVector;
	CurrentWiggleVector = FVector::ZeroVector;
	StartWiggleVector = FVector::ZeroVector;
	CurrentWiggleAlpha = 0.0f;
}
