// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterRotationComponent.h"
#include "Character/CharacterBase.h"
#include "Components/SceneComponent.h"
#include "Character/CharacterBase.h"
#include "Weapon/WeaponBase.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"
#include "Components/SkeletalMeshComponent.h"

// Sets default values for this component's properties
UCharacterRotationComponent::UCharacterRotationComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	OwnerCharacter = Cast<ACharacterBase>(GetOwner());
}

void UCharacterRotationComponent::AddPitchRotation(const float Rate)
{
	NeedRotation.Pitch += Rate * TurnRate * GetWorld()->GetDeltaSeconds();
}

void UCharacterRotationComponent::AddYawRotation(const float Rate)
{
	NeedRotation.Yaw += Rate * TurnRate * GetWorld()->GetDeltaSeconds();
}

void UCharacterRotationComponent::AddRollRotation(const float Rate)
{
	NeedRotation.Roll += Rate * TurnRate * GetWorld()->GetDeltaSeconds();
}

void UCharacterRotationComponent::TryRoundRotation()
{
	if (FMath::Abs(NeedRotation.Pitch) < RotationTolerance && NeedRotation.Pitch != 0)
		NeedRotation.Pitch = 0;
	if (FMath::Abs(NeedRotation.Yaw) < RotationTolerance && NeedRotation.Yaw != 0)
		NeedRotation.Yaw = 0;
	if (FMath::Abs(NeedRotation.Roll) < RotationTolerance && NeedRotation.Roll != 0)
		NeedRotation.Roll = 0;
}

// Called when the game starts
void UCharacterRotationComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UCharacterRotationComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	if (OwnerCharacter)
	{
		if (!NeedRotation.Equals(FRotator::ZeroRotator, RotationTolerance))
		{

			const FRotator OldRotation = NeedRotation;
			const float SmoothAlpha = FMath::Clamp(SmoothLookSensitivity * DeltaTime, 0.0f, 1.0f);
			NeedRotation = bSmoothLook ? FMath::Lerp(NeedRotation, FRotator::ZeroRotator, SmoothAlpha) : FRotator::ZeroRotator;
			TryRoundRotation();
			DeltaRotation = OldRotation - NeedRotation;
			OwnerCharacter->AddControllerPitchInput(DeltaRotation.Pitch);
			OwnerCharacter->AddControllerYawInput(DeltaRotation.Yaw);
			OwnerCharacter->AddControllerRollInput(DeltaRotation.Roll);
		}
	}
}


