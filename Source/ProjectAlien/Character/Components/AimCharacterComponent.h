// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AimCharacterComponent.generated.h"


UENUM()
enum class EAimState : uint8
{
	EAimStart,
	EAimStop,
	EAimWiggle,
	EAimNone
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROJECTALIEN_API UAimCharacterComponent : public UActorComponent
{
	GENERATED_BODY()
		UPROPERTY(VisibleInstanceOnly, Category = AimDefault)
		float DefaultCameraFOV;
	UPROPERTY(VisibleInstanceOnly, Category = AimDefault)
		float DefaultMovementSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Aim, meta = (AllowPrivateAccess = "true"))
		float AimingStopThreshold = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Aim, meta = (AllowPrivateAccess = "true"))
		float AimingSpeed = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Aim, meta = (AllowPrivateAccess = "true"))
		float AimMovementSpeed = 300.0f;

	/**
	 * Using WiggleCurve instead of WiggleSpeed
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Wiggle, meta = (AllowPrivateAccess = "true"))
		uint8 bUseWiggleCurve : 1;
	/**
	 * Speed of weapon wiggle from side to side
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Wiggle, meta = (AllowPrivateAccess = "true"))
		float WiggleSpeed = 1.0f;
	/**
	 * Duration of weapon wiggle from side to side
	 */
	UPROPERTY(EditAnywhere, Category = Wiggle)
		class UCurveFloat* WiggleCurve;
	/**
	 * Using for comparing current weapon wiggle and target one
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Wiggle, meta = (AllowPrivateAccess = "true"))
		float WiggleTolerance = 0.1f;

	class APlayableCharacter* OwnerCharacter;
public:
	// Sets default values for this component's properties
	UAimCharacterComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	UFUNCTION(BlueprintCallable, Category = Aim)
		void StartSighting();

	UFUNCTION(BlueprintCallable, Category = Aim)
		void StopSighting();

private:

	void ProcessAiming();
	void ProcessStartAiming();
	void ProcessStopAiming();
	void ProcessWiggleAiming();
	void CalculateWiggleTarget();
	void UpdateAiming(EAimState NextState);
	void CalculateWiggleSpeed();
	void ResetCachedValues();
	
	float TargetCameraFOV;
	FVector TargetWeaponLocation;

	FVector TargetWiggleVector;
	FVector CurrentWiggleVector;
	FVector StartWiggleVector;
	float CurrentWiggleSpeed;
	float CurrentWiggleAlpha;

	EAimState AimState = EAimState::EAimNone;
};
