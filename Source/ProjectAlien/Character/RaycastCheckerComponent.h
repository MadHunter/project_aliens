// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "RaycastCheckerComponent.generated.h"



UCLASS()
class PROJECTALIEN_API URaycastCheckerComponent : public USceneComponent
{
	GENERATED_BODY()
		UPROPERTY(EditAnywhere)
		float MaxDistance;
	UPROPERTY(EditAnywhere)
		uint8 bDebug : 1;
	UPROPERTY(EditAnywhere)
		uint8 bShowHitName : 1;

public:
	// Sets default values for this component's properties
	URaycastCheckerComponent();
	/**Return either ImpactPoint or EndRayPoint unless collide something*/
	UFUNCTION(BlueprintCallable)
		FVector GetTargetPoint(FRotator RotationOffset = FRotator::ZeroRotator) const;
	UFUNCTION(BlueprintCallable)
		bool HasTargetPoint(/*OUT*/FHitResult& OutHitResult, FRotator RotationOffset) const;

	FORCEINLINE float GetMaxDistance() const { return MaxDistance; }

private:
	class ACharacterBase* OwnerCharacter;
	FRotator RotationOffset;
};
