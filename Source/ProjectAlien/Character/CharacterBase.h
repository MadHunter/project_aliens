// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Types/Changeable.h"
#include "Types/BulletData.h"
#include "CharacterBase.generated.h"


UCLASS()
class PROJECTALIEN_API ACharacterBase : public ACharacter
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditDefaultsOnly, Category = Character)
		float MaxHealth;
	UPROPERTY(EditDefaultsOnly, Category = Character)
		float ScatterMultiplier = 0.2f;
	TChangeable<float> CurrentHealth;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Character)
		class AWeaponBase* CurrentWeapon;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Character)
		class URaycastCheckerComponent* EyeRaycast;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Character)
		class UCharacterRotationComponent* RotationComponent;

	class UCharacterAnimInstance* AnimationInstance;

public:
	DECLARE_EVENT_OneParam(ACharacterBase, FDamageEvent, const float)
	ACharacterBase();
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void TakeDamage(const FBulletDamageData& Damage, FBulletHitData& OutHitData);

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void EquipWeapon(AWeaponBase* Weapon);

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void OnEquipComplete();

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void UnequipWeapon();

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void OnUnequipComplete();

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void MoveForward(float Rate);

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void MoveRight(float Rate);

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void TurnCharacter(float Rate);

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void AimYaw(float Rate);

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void AimPitch(float Rate);

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void StartShooting();

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void StopShooting();

	virtual void Jump() override;

	virtual void StopJumping() override;

	virtual void Crouch(bool bClientSimulation = false) override;
	virtual void UnCrouch(bool bClientSimulation = false) override;


	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void StartRunning();

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void StopRunning();

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void StartSighting();

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void StopSighting();

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void StartReloading();
	
	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void OnReloadComplete();

	FORCEINLINE AWeaponBase* GetWeapon() const { return CurrentWeapon; }
	FORCEINLINE URaycastCheckerComponent* GetEyeRaycast() const { return EyeRaycast; }
	FORCEINLINE float GetCurrentHealth() const { return CurrentHealth; }
	FORCEINLINE UCharacterAnimInstance* GetAnimationInstance() const { return AnimationInstance; }
	FORCEINLINE UCharacterRotationComponent* GetRotationComponent() const { return RotationComponent; }
	FORCEINLINE float GetCharacterScatter() const;

	FORCEINLINE FDamageEvent& OnGetDamage() { return OnGetDamageEvent; }
private:
	FDamageEvent OnGetDamageEvent;
};
