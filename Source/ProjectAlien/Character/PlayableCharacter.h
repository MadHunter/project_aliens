// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/CharacterBase.h"
#include "PlayableCharacter.generated.h"

/**
 *
 */
UCLASS()
class PROJECTALIEN_API APlayableCharacter : public ACharacterBase
{
	GENERATED_BODY()

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class URecoilCharacterComponent* RecoilCharacterComponent;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* HeadComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UAimCharacterComponent* AimCharacterComponent;
	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FirstPersonCameraComponent;
protected:

	virtual void BeginPlay() override;

public:
	APlayableCharacter();
	virtual void Tick(float DeltaTime) override;
	void EquipWeapon(AWeaponBase* Weapon) override;
	void FaceRotation(FRotator NewControlRotation, float DeltaTime) override;
	virtual FRotator GetViewRotation() const override;
	/** Returns	Pawn's eye location */
	virtual FVector GetPawnViewLocation() const override;

	virtual void StartSighting() override;
	virtual void StopSighting() override;
	
	FORCEINLINE UCameraComponent* GetCameraComponent()const { return FirstPersonCameraComponent; }
	FORCEINLINE UAimCharacterComponent* GetAimCharacterComponent()const { return AimCharacterComponent; }
	FORCEINLINE class AProjectAlienHUD* GetPlayerHUD()const { return PlayerHUD; }
protected:
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	void SwitchFireMode();
	void SwitchCrouch();
private:
	FRotator PrevHandRotation;
	AProjectAlienHUD* PlayerHUD;
};
