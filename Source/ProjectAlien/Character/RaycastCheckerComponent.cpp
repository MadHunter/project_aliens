// Fill out your copyright notice in the Description page of Project Settings.


#include "RaycastCheckerComponent.h"
#include "Character/CharacterBase.h"
#include "Weapon/WeaponBase.h"
#include "Engine.h"
#include "Runtime/Engine/Public/CollisionQueryParams.h"

// Sets default values for this component's properties
URaycastCheckerComponent::URaycastCheckerComponent() : MaxDistance(100)
{
	PrimaryComponentTick.bCanEverTick = false;
	OwnerCharacter = Cast<ACharacterBase>(GetOwner());
}

FVector URaycastCheckerComponent::GetTargetPoint(FRotator RotationOffset) const
{
	FHitResult OutHitResult;
	bool bHit = HasTargetPoint(OutHitResult, RotationOffset);
	FVector EndPoint;
	if (bHit)
	{
		EndPoint = OutHitResult.ImpactPoint;
	}
	else
	{
		FRotator Rotation = GetComponentRotation() + RotationOffset;
		EndPoint = GetComponentLocation() + (Rotation.Vector() * MaxDistance);
	}
	if (bDebug)
		DrawDebugLine(GetWorld(), GetComponentLocation(), EndPoint, FColor::Red, true);
	if (bShowHitName && bHit)
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, OutHitResult.Actor->GetName());
	return EndPoint;
}

bool URaycastCheckerComponent::HasTargetPoint(FHitResult& OutHitResult, FRotator RotationOffset) const
{
	FVector Start = GetComponentLocation();
	FRotator Rotation = GetComponentRotation() + RotationOffset;
	FVector End = Start + (Rotation.Vector() * MaxDistance);
	FCollisionQueryParams TraceParams;
	if (OwnerCharacter)
	{
		TraceParams.AddIgnoredActor(OwnerCharacter);
		if (OwnerCharacter->GetWeapon())
		{
			TraceParams.AddIgnoredActor(OwnerCharacter->GetWeapon());
		}
	}
	return GetWorld()->LineTraceSingleByChannel(/*out*/OutHitResult, Start, End, ECC_Camera, TraceParams);

}
