// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayableCharacter.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Math/UnrealMath.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"
#include "Weapon/WeaponBase.h"
#include "ProjectAlienHUD.h"
#include "GameFramework/PlayerController.h"
#include "ui/WeaponViewWidget.h"
#include "Weapon/BulletBase.h"
#include "Character/RaycastCheckerComponent.h"
#include "Components/RecoilCharacterComponent.h"
#include "Components/AimCharacterComponent.h"
#include "Components/CharacterRotationComponent.h"


APlayableCharacter::APlayableCharacter()
{
	HeadComponent = CreateDefaultSubobject<USceneComponent>("Head");
	HeadComponent->SetupAttachment(GetCapsuleComponent());
	HeadComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f);

	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(HeadComponent);
	FirstPersonCameraComponent->bUsePawnControlRotation = false;

	GetMesh()->SetupAttachment(FirstPersonCameraComponent);
	GetMesh()->bCastDynamicShadow = false;
	GetMesh()->CastShadow = false;
	FAttachmentTransformRules Rules(EAttachmentRule::SnapToTarget, true);
	EyeRaycast->AttachToComponent(FirstPersonCameraComponent, Rules);

	RecoilCharacterComponent = CreateDefaultSubobject<URecoilCharacterComponent>("RecoilCharacterComponent");
	AimCharacterComponent = CreateDefaultSubobject<UAimCharacterComponent>("AimCharacterComponent");
}

void APlayableCharacter::BeginPlay()
{
	Super::BeginPlay();
	PlayerHUD = Cast<AProjectAlienHUD>(Cast<APlayerController>(GetController())->GetHUD());
}

void APlayableCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlayableCharacter::EquipWeapon(AWeaponBase* Weapon)
{
	if (Weapon)
	{
		Super::EquipWeapon(Weapon);
		Weapon->OnRecoil().AddUObject(RecoilCharacterComponent, &URecoilCharacterComponent::MakeRecoil);
		AProjectAlienHUD* PlayerHud = Cast<AProjectAlienHUD>(Cast<APlayerController>(GetController())->GetHUD());
		if (PlayerHud && PlayerHud->GetWeaponView())
		{
			PlayerHud->GetWeaponView()->ShowMaxMagSize(Weapon->GetMagMaxSize());
			PlayerHud->GetWeaponView()->ShowCurrentMagSize(Weapon->CurMagSize);
			Weapon->OnBulletHit().AddUObject(PlayerHud->GetWeaponView(), &UWeaponViewWidget::ShowCrosshairHit);
			Weapon->CurMagSize.OnValueChanged().AddUObject(PlayerHud->GetWeaponView(), &UWeaponViewWidget::ShowCurrentMagSize);
			Weapon->CurrentScatter.OnValueChanged().AddUObject(PlayerHud->GetWeaponView(), &UWeaponViewWidget::ShowCrosshairScatter);
		}
	}
}

void APlayableCharacter::FaceRotation(FRotator NewControlRotation, float DeltaTime)
{
	const FRotator CurrentRotation = GetActorRotation();

	const FRotator NewCharacterRotation(CurrentRotation.Pitch, NewControlRotation.Yaw, CurrentRotation.Roll);
	const FRotator NewHeadRotation(NewControlRotation.Pitch, CurrentRotation.Yaw, CurrentRotation.Roll);

	SetActorRotation(NewCharacterRotation);
	if (HeadComponent)
		HeadComponent->SetWorldRotation(NewHeadRotation);

}

FRotator APlayableCharacter::GetViewRotation() const
{
	return GetEyeRaycast()->GetComponentRotation();
}

FVector APlayableCharacter::GetPawnViewLocation() const
{
	return GetEyeRaycast()->GetComponentLocation();
}

void APlayableCharacter::StartSighting()
{
	if (GetWeapon() && !GetWeapon()->IsWeaponLocked())
	{
		Super::StartSighting();
		AimCharacterComponent->StartSighting();
	}
}

void APlayableCharacter::StopSighting()
{
	Super::StopSighting();
	AimCharacterComponent->StopSighting();
}

void APlayableCharacter::SetupPlayerInputComponent(UInputComponent* InputComponent)
{
	check(InputComponent);

	InputComponent->BindAxis("MoveForward", this, &ACharacterBase::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ACharacterBase::MoveRight);

	InputComponent->BindAction("Sprint", IE_Pressed, this, &ACharacterBase::StartRunning);
	InputComponent->BindAction("Sprint", IE_Released, this, &ACharacterBase::StopRunning);

	InputComponent->BindAction("Crouch", IE_Pressed, this, &APlayableCharacter::SwitchCrouch);

	InputComponent->BindAction("Jump", IE_Pressed, this, &ACharacterBase::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &ACharacterBase::StopJumping);

	InputComponent->BindAction("Fire", IE_Pressed, this, &ACharacterBase::StartShooting);
	InputComponent->BindAction("Fire", IE_Released, this, &ACharacterBase::StopShooting);

	InputComponent->BindAction("Aim", IE_Pressed, this, &ACharacterBase::StartSighting);
	InputComponent->BindAction("Aim", IE_Released, this, &ACharacterBase::StopSighting);

	InputComponent->BindAction("Reload", IE_Pressed, this, &ACharacterBase::StartReloading);

	InputComponent->BindAxis("Turn", GetRotationComponent(), &UCharacterRotationComponent::AddYawRotation);
	InputComponent->BindAxis("LookUp", GetRotationComponent(), &UCharacterRotationComponent::AddPitchRotation);

}

void APlayableCharacter::SwitchFireMode()
{
}
void APlayableCharacter::SwitchCrouch()
{
	if (!bIsCrouched)
		Crouch(false);
	else
		UnCrouch(false);
}