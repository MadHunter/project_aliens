// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/CharacterBase.h"
#include "Math/UnrealMathUtility.h"
#include "Components/CapsuleComponent.h"
#include "Animation/AnimInstance.h"
#include "Animation/CharacterAnimInstance.h"
#include "Weapon/WeaponBase.h"
#include "Character/RaycastCheckerComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"
#include "GameFramework/PlayerController.h"
#include "Components/CharacterRotationComponent.h"

// Sets default values
ACharacterBase::ACharacterBase()
{
	EyeRaycast = CreateDefaultSubobject<URaycastCheckerComponent>("EyeRaycast");
	RotationComponent = CreateDefaultSubobject<UCharacterRotationComponent>("CharacterRotationComponent");
	FAttachmentTransformRules Rules(EAttachmentRule::SnapToTarget, true);
	EyeRaycast->SetupAttachment(GetRootComponent(), TEXT("EyeSocket"));
}

void ACharacterBase::BeginPlay()
{
	Super::BeginPlay();
	CurrentHealth = MaxHealth;
	AnimationInstance = Cast<UCharacterAnimInstance>(GetMesh()->GetAnimInstance());
	check(AnimationInstance)
}


void ACharacterBase::TakeDamage(const FBulletDamageData& Damage, FBulletHitData& OutHitData)
{
	float ApplyedDamage = FMath::Min(Damage.BulletDamage, (float)CurrentHealth);
	CurrentHealth -= ApplyedDamage;
	if (CurrentHealth <= 0)
	{
		OutHitData.bLethal = true;
		//DoDeath()
	}
	OutHitData.CausedDamage = ApplyedDamage;
	OnGetDamage().Broadcast(ApplyedDamage);
}

void ACharacterBase::EquipWeapon(AWeaponBase* Weapon)
{
	if (Weapon)
	{
		if (Weapon == CurrentWeapon)
		{
			return;
		}
		else
		{
			if (CurrentWeapon)
			{
				UnequipWeapon();
			}
			CurrentWeapon = Weapon;
			Weapon->OnMagazineEmpty().AddUObject(this, &ACharacterBase::StartReloading);
			Weapon->Equip(this);
			Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules(/*InLocationRule*/ EAttachmentRule::SnapToTarget, /*InRotationRule*/ EAttachmentRule::SnapToTarget, /*InScaleRule*/ EAttachmentRule::KeepRelative, true), TEXT("GripPoint"));
			if (AnimationInstance)
			{
				AnimationInstance->bEquip = true;
			}
		}
	}
}

void ACharacterBase::OnEquipComplete()
{
	if (AnimationInstance)
	{
		AnimationInstance->bEquip = false;
	}
}

void ACharacterBase::UnequipWeapon()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Unequip();
		CurrentWeapon->DetachFromActor(FDetachmentTransformRules::KeepRelativeTransform);
		CurrentWeapon = nullptr;
		if (AnimationInstance)
		{
			AnimationInstance->bIronsight = false;
			AnimationInstance->bReload = false;
			AnimationInstance->bFire = false;
			AnimationInstance->bUnequip = true;
		}
	}
}

void ACharacterBase::OnUnequipComplete()
{
	if (AnimationInstance)
	{
		AnimationInstance->bUnequip = false;
	}
}

void ACharacterBase::MoveForward(const float Rate)
{
	AddMovementInput(GetActorForwardVector(), Rate);
	AnimationInstance->ForwardDirection = Rate;
}

void ACharacterBase::MoveRight(const float Rate)
{
	AddMovementInput(GetActorRightVector(), Rate);
	AnimationInstance->RightDirection = Rate;
}

void ACharacterBase::TurnCharacter(const float Rate)
{
}

void ACharacterBase::AimYaw(const float Rate)
{
	if (AnimationInstance)
	{
		AnimationInstance->AimYaw = Rate;
	}
}

void ACharacterBase::AimPitch(const float Rate)
{
	if (AnimationInstance)
	{
		AnimationInstance->AimPitch = Cast<APlayerController>(GetController())->RotationInput.Pitch;
	}
}

void ACharacterBase::StartShooting()
{
	if (GetWeapon() && !GetWeapon()->IsWeaponLocked())
	{
		GetWeapon()->StartShooting();
		if (AnimationInstance)
		{
			AnimationInstance->bFire = true;
		}
	}
}

void ACharacterBase::StopShooting()
{
	if (GetWeapon())
	{
		GetWeapon()->StopShooting();
	}
	if (AnimationInstance)
	{
		AnimationInstance->bFire = false;
	}
}

void ACharacterBase::Jump()
{
	Super::Jump();
	if (AnimationInstance)
	{
		AnimationInstance->bJump = true;
	}
}

void ACharacterBase::StopJumping()
{
	Super::StopJumping();
	if (AnimationInstance)
	{
		AnimationInstance->bJump = false;
	}
}

void ACharacterBase::Crouch(const bool bClientSimulation)
{
	Super::Crouch(bClientSimulation);
	if (AnimationInstance)
	{
		AnimationInstance->bCrouch = true;
	}
}

void ACharacterBase::UnCrouch(const bool bClientSimulation)
{
	Super::UnCrouch(bClientSimulation);
	if (AnimationInstance)
	{
		AnimationInstance->bCrouch = false;
	}
}

void ACharacterBase::StartRunning()
{
	/*GetCharacterMovement()->UnCrouch();
	GetCharacterMovement()->MovementMode = MOVE_Custom;
	if (AnimationInstance)
	{
		AnimationInstance->bRun = true;
	}*/
}

void ACharacterBase::StopRunning()
{
	GetCharacterMovement()->MovementMode = EMovementMode::MOVE_Walking;
	if (AnimationInstance)
	{
		AnimationInstance->bRun = false;
	}
}

void ACharacterBase::StartSighting()
{
	if (GetWeapon() && !GetWeapon()->IsWeaponLocked())
	{
		CurrentWeapon->SetAiming(true);
		if (AnimationInstance)
		{
			AnimationInstance->bIronsight = true;
		}

	}
}

void ACharacterBase::StopSighting()
{
	if (AnimationInstance)
	{
		AnimationInstance->bIronsight = false;
	}
	if (GetWeapon())
	{
		CurrentWeapon->SetAiming(false);
	}
}

void ACharacterBase::StartReloading()
{
	if (GetWeapon() && !GetWeapon()->IsWeaponLocked() && GetWeapon()->CurMagSize < GetWeapon()->GetMagMaxSize())
	{
		StopShooting();
		/*TEMP*/StopSighting();
		GetWeapon()->LockWeapon();
		if (AnimationInstance)
		{
			AnimationInstance->bReload = true;
			AnimationInstance->LeftHandIKAlpha = 0.0f;
		}
	}
}

void ACharacterBase::OnReloadComplete()
{
	GetWeapon()->CurMagSize = GetWeapon()->GetMagMaxSize();
	if (AnimationInstance)
	{
		AnimationInstance->bReload = false;
		AnimationInstance->LeftHandIKAlpha = 1.0f;
	}
	GetWeapon()->UnlockWeapon();
}

float ACharacterBase::GetCharacterScatter() const
{
	return  GetVelocity().Size() * 0.01f * ScatterMultiplier;
}
