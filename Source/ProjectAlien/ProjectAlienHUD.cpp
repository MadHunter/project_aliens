// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "ProjectAlienHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "UI/WeaponViewWidget.h"


void AProjectAlienHUD::BeginPlay()
{
	Super::BeginPlay();
	WeaponViewWidget = CreateWidget<UWeaponViewWidget>(GetWorld(), WeaponViewWidgetClass);
	if (GetWeaponView())
	{
		GetWeaponView()->AddToViewport();
	}
}

AProjectAlienHUD::AProjectAlienHUD()
{
}


