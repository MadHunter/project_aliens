// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Types/Changeable.h"
#include "ScatterParams.generated.h"

/**
 *
 */
USTRUCT(BlueprintType)
struct PROJECTALIEN_API FScatterParams
{
	GENERATED_USTRUCT_BODY()
		/**
		 * Minimal scatter
		 */
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MinValue;
	/**
	 * Maximum scatter
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MaxValue;
	/**
	 * Relax scatter speed while shooting
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float RelaxSpeedWhileShooting;
	/**
	 * Relax scatter speed idle
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float RelaxSpeed;
	/*
	 * Base increase scatter after shot
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin = 0))
		float IncPerShot;
	/**
	 * Increase IncPerShot after shot
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin = 0))
		float IncAcceleration;
	/**
	 * Maximum IncPerShot value
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin = 0))
		float MaxIncrease;
	/**
	 * Relax IncPerShot speed while shooting
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float IncreaseRelaxSpeedWhileShooting;
	/**
	 * Relax IncPerShot speed idle
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float IncreaseRelaxSpeed;

	FScatterParams() : MinValue(0.1f), MaxValue(1.0f), RelaxSpeedWhileShooting(2.0f), RelaxSpeed(5.0f),
		IncPerShot(0.1f), IncAcceleration(0.1f), MaxIncrease(0.5f), IncreaseRelaxSpeedWhileShooting(1.0f), IncreaseRelaxSpeed(5.0f)
	{
	}
};
