// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FireParams.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct PROJECTALIEN_API FFireParams
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bShooting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float LastShotTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float ShotsPerMinute;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int UninterruptedShots;

	FFireParams() : bShooting(false), LastShotTime(0), ShotsPerMinute(60), UninterruptedShots(0) {}
};

UENUM(BlueprintType, meta = (Bitflags))
enum class EFireMode : uint8
{
	OneShot = 0x01,
	TwoShot = 0x02,
	ThreeShot = 0x04,
	Auto = 0x08
};
ENUM_CLASS_FLAGS(EFireMode)
