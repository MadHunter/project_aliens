// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


/**
 * Wrapper for a value which can be dispatched for changing
 */

template <typename TValue>
class PROJECTALIEN_API TChangeable
{
public:
	DECLARE_EVENT_OneParam(TChangeable<TValue>, FChangeEvent, TValue)
	TChangeable<TValue>() { Value = TValue(); }
	TChangeable<TValue>(TValue Obj) { Value = Obj; }
	TChangeable<TValue>(TChangeable<TValue>& Obj) = default;
	TChangeable<TValue>(TChangeable<TValue>&& Obj) noexcept = default;

	~TChangeable<TValue>()
	{
		OnValueChanged().Clear();
	}
	
	TChangeable<TValue>& operator = (TValue obj)
	{
		OldValue = Value;
		Value = obj;
		if (OldValue != Value)
			OnValueChanged().Broadcast(Value);
		return *this;
	}

	TChangeable<TValue>& operator = (const TChangeable<TValue> & Obj)
	{
		OldValue = Value;
		Value = Obj.Value;
		if (OldValue != Value)
			OnValueChanged().Broadcast(Value);
		return *this;
	}

	TChangeable<TValue>& operator = (TChangeable<TValue> && Obj) = delete;
	TChangeable<TValue>& operator+=(const TValue & rhs)
	{
		OldValue = Value;
		Value += rhs;
		if (OldValue != Value)
			OnChangeEvent.Broadcast(Value);
		return *this;
	}
	TChangeable<TValue>& operator-=(const TValue & rhs)
	{
		OldValue = Value;
		Value -= rhs;
		if (OldValue != Value)
			OnChangeEvent.Broadcast(Value);
		return *this;
	}
	TChangeable<TValue>& operator*=(const TValue & rhs)
	{
		OldValue = Value;
		Value *= rhs;
		if (OldValue != Value)
			OnChangeEvent.Broadcast(Value);
		return *this;
	}
	TChangeable<TValue>& operator/=(const TValue & rhs)
	{
		OldValue = Value;
		Value /= rhs;
		if (OldValue != Value)
			OnChangeEvent.Broadcast(Value);
		return *this;
	}
	TChangeable<TValue>& operator++()
	{
		OnChangeEvent.Broadcast(++Value);
		return *this;
	}
	TChangeable<TValue> operator++(int Arg)
	{
		TChangeable<TValue> Old = Value;
		OnChangeEvent.Broadcast(++Value);
		return Old;
	}

	TChangeable<TValue>& operator--()
	{
		OnChangeEvent.Broadcast(--Value);
		return *this;
	}
	TChangeable<TValue> operator--(int Arg)
	{
		TChangeable<TValue> Old = Value;
		OnChangeEvent.Broadcast(--Value);
		return Old;
	}

	TChangeable<TValue> operator-()
	{
		TChangeable<TValue> Obj = -Value;
		return Obj;
	}

	operator TValue() { return Value; }
	operator TValue() const { return Value; }


	FChangeEvent& OnValueChanged() { return OnChangeEvent; }
private:

	TValue Value;
	TValue OldValue;
	FChangeEvent OnChangeEvent;
};
