// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BulletData.generated.h"


USTRUCT(BlueprintType)
struct FBulletHitData
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = BulletData)
		float CausedDamage;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = BulletData)
		uint8 bWeakPoint : 1;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = BulletData)
		uint8 bLethal : 1;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = BulletData)
		uint8 bRicochet : 1;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = BulletData)
		uint8 bPierced : 1;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = BulletData)
		uint8 bStopped : 1;
	FBulletHitData() : CausedDamage(0), bWeakPoint(false), bLethal(false), bRicochet(false), bPierced(false), bStopped(false) {}
};

DECLARE_EVENT_OneParam(ABulletBase, FBulletHitEvent, const FBulletHitData&)

USTRUCT(BlueprintType)
struct FBulletSpawnParams
{
	GENERATED_BODY()
		UPROPERTY(BlueprintReadWrite, Category = BulletData)
		class ACharacterBase* Shooter;
	UPROPERTY(BlueprintReadWrite, Category = BulletData)
		FVector SpawnPosition;
	UPROPERTY(BlueprintReadWrite, Category = BulletData)
		FRotator Rotation;
	UPROPERTY(BlueprintReadWrite, Category = BulletData)
		float Scatter;
	UPROPERTY(BlueprintReadWrite, Category = BulletData)
		float Damage;
	UPROPERTY(BlueprintReadWrite, Category = BulletData)
		float StartSpeed;
	UPROPERTY(BlueprintReadWrite, Category = BulletData)
		float PiercingPower;
	UPROPERTY(BlueprintReadWrite, Category = BulletData)
		float StoppingPower;
	UPROPERTY(BlueprintReadWrite, Category = BulletData)
		float HitImpusle;
	FBulletHitEvent OnBulletHit;
};

USTRUCT(BlueprintType)
struct FBulletDamageData
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = BulletData)
		float BulletDamage;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = BulletData)
		float HitImpulse;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = BulletData)
		float PiercingPower;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = BulletData)
		float StoppingPower;
	FBulletDamageData() : BulletDamage(0), HitImpulse(0), PiercingPower(0), StoppingPower(0) {}
};
