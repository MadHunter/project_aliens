// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterAnimInstance.h"
#include "Character/CharacterBase.h"
#include "Weapon/WeaponBase.h"


FVector UCharacterAnimInstance::GetWeaponLeftHandPoint() const
{
	if (OwnerCharacter)
	{
		const FTransform GripTransform = OwnerCharacter->GetMesh() ? OwnerCharacter->GetMesh()->GetSocketTransform("GripPoint", RTS_Component) : OwnerCharacter->GetActorTransform();
		const FTransform LeftHandTransform = (OwnerCharacter->GetWeapon() && OwnerCharacter->GetWeapon()->GetMesh()) ? OwnerCharacter->GetWeapon()->GetMesh()->GetSocketTransform("LeftHand", RTS_Component) : OwnerCharacter->GetActorTransform();
		const FVector GripLocation = GripTransform.GetLocation();
		const FQuat GripRotation = GripTransform.GetRotation();
		FVector LeftHandLocation = LeftHandTransform.GetLocation();
		if (OwnerCharacter->GetWeapon())
			LeftHandLocation += OwnerCharacter->GetWeapon()->GetRootComponent()->RelativeLocation;

		return GripLocation + GripRotation.Rotator().RotateVector(LeftHandLocation);
	}
	return FVector::ZeroVector;
}

float UCharacterAnimInstance::GetShootingAnimSpeed() const
{
	if (OwnerCharacter && OwnerCharacter->GetWeapon())
	{
		return OwnerCharacter->GetWeapon()->GetShotsPerMinute() / 60.0f;
	}
	return 1.0f;
}

bool UCharacterAnimInstance::HasWeapon() const
{
	return OwnerCharacter && OwnerCharacter->GetWeapon();
}

void UCharacterAnimInstance::NativeInitializeAnimation()
{
	OwnerCharacter = Cast<ACharacterBase>(TryGetPawnOwner());
}
