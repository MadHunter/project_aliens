// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "CharacterAnimInstance.generated.h"

/**
 *
 */
UCLASS()
class PROJECTALIEN_API UCharacterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Character)
		class ACharacterBase* OwnerCharacter;
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterAnimation)
		float ForwardDirection;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterAnimation)
		float RightDirection;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterAnimation)
		float TurnBody;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterAnimation)
		float AimYaw;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterAnimation)
		float AimPitch;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterAnimation)
		float LeftHandIKAlpha = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterAnimation)
		uint32 bIronsight : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterAnimation)
		uint32 bFire : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterAnimation)
		uint32 bCrouch : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterAnimation)
		uint32 bJump : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterAnimation)
		uint32 bReload : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterAnimation)
		uint32 bRun : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterAnimation)
		uint32 bEquip : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterAnimation)
		uint32 bUnequip : 1;

	/**
	 * Gets IK point for left hand when weapon is equiped
	 */
	UFUNCTION(BlueprintCallable, Category = Animation)
		FVector GetWeaponLeftHandPoint() const;
	/**
	 * Get animation speed depend on ShootPerMinute parameter 
	 */
	UFUNCTION(BlueprintCallable, Category = Animation)
		float GetShootingAnimSpeed() const;

	UFUNCTION(BlueprintCallable, Category = Animation)
		bool HasWeapon() const;

	virtual void NativeInitializeAnimation() override;
};
